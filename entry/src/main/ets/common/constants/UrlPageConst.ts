/**
 * @author Hrbing
 * @date 2024/5/16--11:39
 *
 * 页面路由的地址
 *
 */
export default class UrlPageConst {
  // 启动页
  static readonly LAUNCHER_SPLASH: string = 'pages/SplashPage'
  // 登录页
  static readonly LAUNCHER_LOGIN: string = ''
  // 主页面
  static readonly LAUNCHER_MAIN: string = 'pages/MainPage'
}